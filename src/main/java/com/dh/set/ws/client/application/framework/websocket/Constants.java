package com.dh.set.ws.client.application.framework.websocket;

/**
 * @author Santiago Mamani
 */
public class Constants {

    private Constants() {
    }

    public static class MessageBroker {
        public static final String DESTINATION_PREFIX = "/smg";
    }

    public static class StompEndpoint {
        public static final String WS_BROKER = "/channel";
    }
}
