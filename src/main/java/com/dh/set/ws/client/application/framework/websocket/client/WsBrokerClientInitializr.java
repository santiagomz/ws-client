package com.dh.set.ws.client.application.framework.websocket.client;

import com.dh.set.ws.client.application.framework.websocket.Constants;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Santiago Mamani
 */
public final class WsBrokerClientInitializr {

    private Integer port = 8080;

    private String host = "localhost";

    private List<String> channels;

    private WebSocketStompClient stompClient;

    private static WsBrokerClientInitializr wsBrokerClientInitializr = null;

    public static WsBrokerClientInitializr getInstance() {
        if (wsBrokerClientInitializr == null) {
            wsBrokerClientInitializr = new WsBrokerClientInitializr();
        }
        return wsBrokerClientInitializr;
    }

    private WsBrokerClientInitializr() {
        channels = new ArrayList<>();
        initializeWebSocket();
    }

    private void initializeWebSocket() {
        WebSocketClient client = new StandardWebSocketClient();
        stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
    }

    public void startNewClient() {
        WsBrokerSessionClient session = new WsBrokerSessionClient();
        session.setChannels(channels);
        stompClient.connect(getUrl(), session);
    }

    private String getUrl() {
        return "ws://" + host + ":" + port + Constants.StompEndpoint.WS_BROKER;
    }

    public void addChannel(String channel) {
        channels.add(channel);
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
