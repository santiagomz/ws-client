package com.dh.set.ws.client.application;

import com.dh.set.ws.client.application.framework.websocket.client.WsBrokerClientInitializr;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Santiago Mamani
 */
@SpringBootApplication
public class WsClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(WsClientApplication.class, args);
        WsBrokerClientInitializr.getInstance().setHost("localhost");
        WsBrokerClientInitializr.getInstance().setPort(8035);
        WsBrokerClientInitializr.getInstance().addChannel("/user/26bc6ea2/role/HR/conversation/53");
        WsBrokerClientInitializr.getInstance().startNewClient();
    }

}
