package com.dh.set.ws.client.application.framework.websocket.client;

import com.dh.set.ws.client.application.framework.websocket.Constants;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * @author Santiago Mamani
 */
public class WsBrokerSessionClient extends StompSessionHandlerAdapter {

    @SuppressWarnings("unused")
    private static Logger LOG = LoggerFactory.getLogger(WsBrokerSessionClient.class);

    @Setter
    private List<String> channels;

    private String sessionId;

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        sessionId = session.getSessionId();
        channels.forEach(channel -> session.subscribe(Constants.MessageBroker.DESTINATION_PREFIX + channel, this));
        LOG.info("[===] New session WebSocket client established with id: " + sessionId + ", finished subscription to the channels");
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        LOG.error("[===] Exception stomp client: ", exception);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return Map.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        LOG.info("[===] WebSocket client sessionId: " + sessionId + " = [destination: " + headers.getDestination() + ", payload: " + payload + "]");
    }
}